﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour
{

    public int Damage = 1;
    public float Speed = 5; 
    public Transform ParrentSpawnPoint { get; set; }

    private float _deltaSpeed;
    private Collider2D _sheelCollider;
    private Vector2 _leftRayOrigin;
    private Vector2 _rightRayOrigin;
    
    // Use this for initialization
    private void Awake()
    {
        _deltaSpeed = Speed * Time.deltaTime;
        _sheelCollider = GetComponent<Collider2D>();

        //Debug.Log(_sheelCollider.bounds.size +" "+ _sheelCollider.bounds.center);   
        _rightRayOrigin = Vector3.right*_sheelCollider.bounds.size.x;
        _leftRayOrigin = Vector3.left * _sheelCollider.bounds.size.x;
    }


    // Update is called once per frame
    private void Update()
    {
        RaycastHit2D raycastHit = Physics2D.Raycast(transform.TransformPoint(_leftRayOrigin), transform.up,_deltaSpeed);
        

        if (raycastHit.collider != null)
        {
            Heals heals = raycastHit.collider.GetComponent<Heals>();

            if (heals != null)
            {
                heals.GetDamage(Damage);
            }

            ResetShell();
        }

        else
        {
            raycastHit = Physics2D.Raycast(transform.TransformPoint(_rightRayOrigin), transform.up ,_deltaSpeed);
            
            if (raycastHit.collider != null)
            {
                Heals heals = raycastHit.collider.GetComponent<Heals>();

                if (heals != null)
                {
                    heals.GetDamage(Damage);
                }

                ResetShell();
            }
            
            else
            {
                transform.position += transform.up * _deltaSpeed;
            }
        }
    }

    private void ResetShell()
    {
        //Time.timeScale = 0;
        gameObject.SetActive(false);
        transform.parent = ParrentSpawnPoint;
        transform.localPosition = Vector3.zero;
    }
}
