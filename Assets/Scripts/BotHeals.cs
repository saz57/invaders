﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotHeals : Heals
{
    private BotController _botController;

    private void Start()
    {
        _botController = GetComponent<BotController>();
    }

    protected override void OnDeath()
    {
        base.OnDeath();
        _botController.OnDeath();
        //Destroy(gameObject);

    }

}
