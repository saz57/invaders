﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotMovement : Movement
{
    public float StepDown;

    public bool GetLeftBound()
    {
        return _getLeftBound;
    }

    public bool GetRightBound()
    {
        return _getRightBound;
    }

    public void MoveDown()
    {
        transform.position += Vector3.down * StepDown;
    }

}
