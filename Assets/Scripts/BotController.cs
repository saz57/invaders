﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotController : MonoBehaviour {

    public Cannon cannon;
    public BotMovement movement;
    public delegate void OnDeathDelegate(BotController botController);
    private event OnDeathDelegate OnDeathEvent;

    public bool GetLeftBound()
    {
        return movement.GetLeftBound();
    }

    public bool GetRightBound()
    {
        return movement.GetRightBound();
    }


    public void AddOnDeathListener(OnDeathDelegate onDeathDelegate)
    {
        OnDeathEvent += onDeathDelegate;
    }

    public void RemoveOnDeathListener(OnDeathDelegate onDeathDelegate)
    {
        OnDeathEvent -= onDeathDelegate;
    }

    public bool Fire()
    {
        return cannon.Fire();
    }


    public void OnDeath()
    {
        if (OnDeathEvent != null)
        {
            OnDeathEvent(this);
        }
    }

    public void OnDestroy()
    {
        OnDeathEvent = null;
    }

    public void MoveDown()
    {
        movement.MoveDown();
    }
    public void Move(float input)
    {
        movement.Move(input);
    }
}
