﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public bool enableControll;
    public PlayerMovement CurrentPlayerMovement;
    public Cannon CurrentCannon;
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (enableControll)
        {
            CurrentPlayerMovement.Move(Input.GetAxis("Horizontal"));

            if (Input.GetButtonDown("Jump"))
            {
                CurrentCannon.Fire();
            }
        }
	}
}
