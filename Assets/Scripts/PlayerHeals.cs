﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHeals : Heals
{
    private GameManager manager;

    private void Start()
    {
        manager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    }

    protected override void OnDeath()
    {
        base.OnDeath();
        manager.GameEnd(false);
    }

}
