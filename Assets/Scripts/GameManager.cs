﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Text messageField;
    public BotManager botManager;
    public PlayerController player;
    public Vector2 ScreenWorldBounds;
    public float ScreenWorldTop; //{ get; private set; }
   // { get; private set; }
    

    // Use this for initialization
    private void Awake()
    {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        }

        player.enableControll = false;

        //ScreenWorldBounds = new Vector2(Camera.main.ScreenToWorldPoint(Vector3.zero).x, Camera.main.ScreenToWorldPoint(Vector3.right * Screen.width).x);
        //ScreenWorldTop = Camera.main.ScreenToWorldPoint(Vector3.up * Screen.height).y;

        messageField.text = string.Empty;
        botManager.Manager = this;
        botManager.Initialise();

        GameObject topBound = new GameObject("TopBound", typeof(BoxCollider2D));
        topBound.transform.position = new Vector3(Mathf.Lerp(ScreenWorldBounds.x, ScreenWorldBounds.y, 0.5f), ScreenWorldTop);
        topBound.transform.localScale = new Vector3(ScreenWorldBounds.x - ScreenWorldBounds.y, 0.1f, 1);
        GameObject buttomBound = new GameObject("ButtomBound", typeof(BoxCollider2D));
        buttomBound.transform.position = new Vector3(Mathf.Lerp(ScreenWorldBounds.x, ScreenWorldBounds.y, 0.5f), Camera.main.ScreenToWorldPoint(Vector3.zero).y);
        buttomBound.transform.localScale = new Vector3(ScreenWorldBounds.x - ScreenWorldBounds.y, 0.1f, 1);

        StartCoroutine(GameStart());
        //topBound.GetComponent<Collider2D>().bounds.SetMinMax(new Vector3(ScreenWorldBounds.x,ScreenWorldTop,0), new Vector3(ScreenWorldBounds.y, ScreenWorldTop, 0));
    }

    public IEnumerator GameStart()
    {
        messageField.text = "3";
        yield return new WaitForSeconds(1);

        messageField.text = "2";
        yield return new WaitForSeconds(1);
        
        messageField.text = "1";
        yield return new WaitForSeconds(1);
        messageField.text = "go";
        botManager.OnGameStart();
        player.enableControll = true;
        yield return new WaitForSeconds(1);
        messageField.text = string.Empty;
    }

    public void GameEnd(bool win)
    {
        Time.timeScale = 0;
        player.enableControll = false;
        if (win)
        {
            messageField.text = "You win";
        }
        else
        {
            messageField.text = "You loose";
        }
    }
}
