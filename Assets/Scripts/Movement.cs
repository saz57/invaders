﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Movement : MonoBehaviour {

    public float Speed = 5;
    [HideInInspector]
    public Vector2 ScreenWorldBounds;

    protected bool _getLeftBound;
    protected bool _getRightBound;

    private Vector2 _worldBounds;
    private float _deltaSpeed;
    
    // Use this for initialization
    public void Start()
    {
       
        ScreenWorldBounds = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().ScreenWorldBounds;
        Collider2D collider = GetComponent<Collider2D>();
        _worldBounds.x = ScreenWorldBounds.x + collider.bounds.size.x;
        _worldBounds.y = ScreenWorldBounds.y - collider.bounds.size.x;
        //_screenWorldSize.x += ;
        _deltaSpeed = Speed * Time.deltaTime;
    }

    public void Move(float input)
    {
        float inputSpeed = _deltaSpeed * input;
        transform.position += Vector3.right * inputSpeed;

        if (transform.position.x > _worldBounds.y)
        {
            transform.position = new Vector3(_worldBounds.y, transform.position.y, transform.position.z);
            _getLeftBound = false;
            _getRightBound = true;

        }

        else if (transform.position.x < _worldBounds.x)
        {
            transform.position = new Vector3(_worldBounds.x, transform.position.y, transform.position.z);
            _getLeftBound = true;
            _getRightBound = false;
        }
        else
        {
            _getLeftBound = false;
            _getRightBound = false;
        }

        //Debug.Log(_getLeftBound + " " + _getRightBound);
    }
}
