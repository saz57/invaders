﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour {

    public GameObject ShellPrefab;
    public Transform ShellSpawnPoint;
    private GameObject _currentShellObject;

    private void Start()
    {
        _currentShellObject = Instantiate(ShellPrefab, ShellSpawnPoint);
        Shell shell = _currentShellObject.GetComponent<Shell>();

        if (shell == null)
        {
            shell = _currentShellObject.AddComponent<Shell>();   
        }

        shell.ParrentSpawnPoint = ShellSpawnPoint;
        _currentShellObject.SetActive(false);
    }

    public bool Fire()
    {
        if(!_currentShellObject.activeSelf)
        {
            _currentShellObject.transform.position = ShellSpawnPoint.position;
            _currentShellObject.transform.parent = null;
            _currentShellObject.SetActive(true);
            return true;
        }
        return false;
    }
}
