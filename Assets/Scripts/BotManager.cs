﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotManager : MonoBehaviour
{
    public float secondsForNewFire = 1;
    public Vector2Int BotLeight;
    public Vector2 BotSpawnOffset;
    public Vector2 OffsetAroundBots;
    public GameObject BotPrefab;

    public GameManager Manager { get; set; }

    private GameObject _botsParrent;
    private float _screenWorldTop;
    private Vector2 _screenWorldBounds;
    private Vector2 _botWorldBounds;
    private BotController[,] _bots;
    private List<FrontBot> _frontLineBots;

    public class FrontBot
    {
        public Vector2Int positionInArray { get; private set; }
        public BotController botController { get; private set; }

        public FrontBot(Vector2Int _positionInArray, BotController _botController)
        {
            positionInArray = _positionInArray;
            botController = _botController;
        }
    }

    private void Start()
    {
        _botsParrent = new GameObject("BotsParrent");

        if (Manager == null)
        {
            GameObject obj = GameObject.FindGameObjectWithTag("GameController");
            GameManager Manager = obj.GetComponent<GameManager>();
        }



        //OnGameStart();
    }

    public void Initialise()
    {
        _screenWorldBounds = Manager.ScreenWorldBounds;
        _screenWorldTop = Manager.ScreenWorldTop;
    }

    /*private void Update()
    {
        foreach (var debugBot in _frontLineBots)
        {
            Debug.DrawRay(debugBot.botController.transform.position, Vector2.down, Color.red);
        }
        
    }*/
    public void OnGameStart()
    {
        SpawnBots();
        StartCoroutine(InGame());
    }

    private IEnumerator InGame()
    {
        StartCoroutine(MoveRight());

        while (_frontLineBots.Count > 0)
        {

            yield return new WaitForSeconds(secondsForNewFire);
            int fireingBotsCount = Random.Range(1, _frontLineBots.Count);

            for (int i = 0; i < fireingBotsCount; i++)
            {
                int currentFireingBot = Random.Range(0, _frontLineBots.Count);
                _frontLineBots[currentFireingBot].botController.Fire();
                /*if(!)
                {
                    if()
                }*/
            }
        }
        Manager.GameEnd(true);
    }

    private IEnumerator MoveLeft()
    {

        bool getBound = false;
        while (!getBound)
        {
            if(_frontLineBots[0]!= null)
            getBound = _frontLineBots[0].botController.GetLeftBound();

            foreach (var bot in _bots)
            {
                if (bot != null)
                {
                    bot.Move(-1);
                    yield return null;
                }
            }
            
        }
        yield return null;
        MoveDown();
        StartCoroutine(MoveRight());
    }


    private IEnumerator MoveRight()
    {
        bool getBound = false;
        while (!getBound)
        {
            getBound = _frontLineBots[_frontLineBots.Count - 1].botController.GetRightBound();
            foreach (var bot in _bots)
            {
                if (bot != null)
                {
                    bot.Move(1);
                    yield return null;
                }
            }
            
        }
        yield return null;
        MoveDown();
        StartCoroutine(MoveLeft());

    }
    private void MoveDown()
    {
        foreach (var bot in _bots)
        {
            if (bot != null)
            {
                bot.MoveDown();
            }
        }
    }

    public void SpawnBots()
    {
        Vector2 botColliderSize = Vector2.zero;
        _bots = new BotController[BotLeight.x, BotLeight.y];
        _frontLineBots = new List<FrontBot>();
        Vector3 currentBotSpawnPosition;
        GameObject.CreatePrimitive(PrimitiveType.Cube).transform.position = new Vector3(_screenWorldBounds.x + BotSpawnOffset.x, _screenWorldTop - BotSpawnOffset.y);


        for (int i = 0; i < BotLeight.x; i++)
        {

            currentBotSpawnPosition = new Vector2(_screenWorldBounds.x + BotSpawnOffset.x, _screenWorldTop - BotSpawnOffset.y) + (OffsetAroundBots + botColliderSize) * Vector2.right * i;

            for (int j = 0; j < BotLeight.y; j++)
            {

                GameObject bot = Instantiate(BotPrefab, currentBotSpawnPosition, BotPrefab.transform.rotation, _botsParrent.transform);
                botColliderSize = bot.GetComponent<Collider2D>().bounds.size;
                //Debug.Log(botColliderSize);
                _bots[i, j] = bot.GetComponent<BotController>();
                _bots[i, j].AddOnDeathListener(BotDestroyed);
                currentBotSpawnPosition += (Vector3)((botColliderSize + OffsetAroundBots) * Vector2.down);
            }
            _frontLineBots.Add(new FrontBot(new Vector2Int(i, _bots.GetLength(1) - 1), _bots[i, _bots.GetLength(1) - 1].GetComponent<BotController>()));

        }

    }

    public void BotDestroyed(BotController bot)
    {
        FrontBot foundBot = _frontLineBots.Find(x => x.botController == bot);
        bool newBotFound = false;
        if (foundBot != null)
        {
            Debug.Log(foundBot.positionInArray);
            Destroy(_bots[foundBot.positionInArray.x, foundBot.positionInArray.y].gameObject);
            _bots[foundBot.positionInArray.x, foundBot.positionInArray.y] = null;

            for (int j = foundBot.positionInArray.y; j >= 0; j--)
            {

                if (_bots[foundBot.positionInArray.x, j] != null)
                {

                    newBotFound = true;
                    _frontLineBots[_frontLineBots.IndexOf(foundBot)] = new FrontBot(new Vector2Int(foundBot.positionInArray.x, j), _bots[foundBot.positionInArray.x, j]);
                    break;
                }

            }

            if (!newBotFound)
            {
                _frontLineBots.Remove(foundBot);
            }


        }
    }

    // Update is called once per frame




}
