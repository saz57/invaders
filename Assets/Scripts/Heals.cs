﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heals : MonoBehaviour
{

    public int MaxHeals = 1;
    private int _currentHeals;
    // Use this for initialization
    void Awake()
    {
        _currentHeals = MaxHeals;
    }

    // Update is called once per frame
    public void GetDamage(int damage)
    {
        _currentHeals -= damage;

        if (_currentHeals <= 0)
        {
            OnDeath();
        }
    }

    protected virtual void OnDeath()
    {
        _currentHeals = 0;
    }
}
